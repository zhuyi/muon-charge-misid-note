%Muon Charge Mis-id Rate Estimation Based on Truth-Reco Matching

Charge mis-id rate in Drell-Yan samples is calculated by matching reconstruted muons to truth muons directly. A dedicated package in ATHENA \cite{ATLAS-TDR-17} is employed to manage the work. The truth-reco pair is constructed following the steps:
\begin{itemize}
\item Extract all reconstructed and truth muons from DAOD files
\item Calculate $\Delta R$ between each reconstructed muon and each truth muon
\item Sort the $\Delta R$ values globally
\item A $\Delta R$ value would be assigned to the related reconstructed-truth muon pair if it is a current global minimum and smaller than 0.025. All other $\Delta R$ values related to any muon of the pair will be removed
\end{itemize}
Thus, one reconstructed muon is matched to a truth muon closest to itself. Three flags are then introduced to describe the charge identification results: 
\begin{itemize}
\item good: if the reconstructed muon shares the same charge with the truth muon matched to it
\item unmatched: if the reconstructed muon failed to be matched to one truth muon
\item mis-charged: if the reconstructed muon has a different charge from the truth muon matched to it
\end{itemize}

\begin{figure}[!h]
\centering
  \subfloat[\label{fig:flag_loose_all_dy}]{\includegraphics[width=0.48\textwidth]{figures/analysis_dy/table_loose_allType_gt20}}
  \subfloat[\label{fig:flag_medium_all_dy}]{\includegraphics[width=0.48\textwidth]{figures/analysis_dy/table_medium_allType_gt20}}

  \subfloat[\label{fig:flag_tight_all_dy}]{\includegraphics[width=0.48\textwidth]{figures/analysis_dy/table_tight_allType_gt20}}
  \caption{Normalized distributions charge matching flag versus \pt of all types of muons at $Loose$ working point (a), $Medium$ working point (b) and $Tight$ working point (c)}
\label{fig:flag_all_dy}
\end{figure}

\begin{figure}[!h]
\centering
  \subfloat[\label{fig:flag_loose_CB_dy}]{\includegraphics[width=0.48\textwidth]{figures/analysis_dy/table_loose_combined_gt20}}
  \subfloat[\label{fig:flag_medium_CB_dy}]{\includegraphics[width=0.48\textwidth]{figures/analysis_dy/table_medium_combined_gt20}}

  \subfloat[\label{fig:flag_tight_CB_dy}]{\includegraphics[width=0.48\textwidth]{figures/analysis_dy/table_tight_combined_gt20}}
  \caption{Normalized distributions charge matching flag versus \pt of CB muons at $Loose$ working point (a), $Medium$ working point (b) and $Tight$ working point (c)}
\label{fig:flag_CB_dy}
\end{figure}

Normalized distributions of the flags versus \pt could be found in Figure \ref{fig:flag_all_dy} for all types of muons and Figure \ref{fig:flag_CB_dy} for CB muons. In Figure \ref{fig:flag_all_dy}, numbers of muons that failed to be matched or matched to a truth one with a flipped charge are significantly suppressed at $Medium$ and $Tight$ working point compared to $Loose$ point, reflecting a better reconstruction performance with more requirements.

Using these flags, muon charge mis-id rate is calculated as:
$$charge\ mis-id\ rate=\frac{number\ of\ mis-charged\ muons}{number\ of\ good\ muons + number\ of\ mis-charged\ muons}$$
in each bin.

Figure \ref{fig:rate_CB_dy} and Table \ref{tab:rate_CB_dy} reveal the mis-id rate across a wide range of \pt. Rates smaller than $1\times10^{-3}$ is found within (20,\ 500) GeV, but it increases to \textasciitilde$10^{-1}$ with muon \pt greater than 1000 GeV. The very high rate indicates great difficulties in correctly reconstructing high \pt muons while a relatively high statistical error result from the low statistics. Furthermore, though no notable difference is found between $Loose$ and $Medium$ muons due to negligible difference in yields that could be seen in Figure \ref{fig:pt_CB_dy} and Figure \ref{fig:flag_CB_dy}, $Tight$ requirements significantly reduce the mis-id charge rate.

\begin{figure}[!ht]
\centering
  \subfloat[\label{fig:rate_loose_CB_dy}]{\includegraphics[width=0.48\textwidth]{figures/analysis_dy/rate_loose_combined_gt20}}
  \subfloat[\label{fig:rate_medium_CB_dy}]{\includegraphics[width=0.48\textwidth]{figures/analysis_dy/rate_medium_combined_gt20}}

  \subfloat[\label{fig:rate_tight_CB_dy}]{\includegraphics[width=0.48\textwidth]{figures/analysis_dy/rate_tight_combined_gt20}}
  \caption{Muon Charge Mis-id rate of CB muons versus \pt at $Loose$ working point (a), $Medium$ working point (b) and $Tight$ working point (c). The rectangles filled with slashes show the statistical errors in each bin}
\label{fig:rate_CB_dy}
\end{figure}

\begin{table}[!ht]
    \centering
    \begin{tabular}{lccccc}
        \toprule
        \pt/GeV & \multicolumn{1}{c}{(20,\ 50)} & \multicolumn{1}{c}{(50,\ 100)} & \multicolumn{1}{c}{(100,\ 500)} & \multicolumn{1}{c}{(500,\ 1000)} & \multicolumn{1}{c}{(1000,\ 5000)} \\ \hline
        \midrule
        $Loose$  & $2.83\times10^{-5}$ & $5.01\times10^{-6}$ & $4.52\times10^{-4}$ & $1.38\times10^{-2}$ & $3.52\times10^{-1}$ \\ \hline
        $Medium$ & $2.83\times10^{-5}$ & $5.01\times10^{-6}$ & $4.52\times10^{-4}$ & $1.38\times10^{-2}$ & $3.56\times10^{-1}$ \\ \hline
        $Tight$  & $8.01\times10^{-7}$ & $3.19\times10^{-7}$ & $9.02\times10^{-5}$ & $9.96\times10^{-3}$ & $2.16\times10^{-1}$ \\ \hline
        \bottomrule
    \end{tabular}
    \caption{CB Muon Charge Mis-id rate versus \pt}
    \label{tab:rate_CB_dy}
\end{table}

Mis-id rates as a function of $\frac{q/p\ err}{q/p}$ are shown in Figure \ref{fig:rate_CB_qop_dy} and Table. Great asymmetries could be observed in the plots in very high $\frac{q/p\ err}{q/p}$ ranges where the reconstruction of momenta are relatively inaccurate. The mis-id rate tends to be higher when q/p error is smaller than 0. But a moderate rate of \textasciitilde$10^{-6}$ could be found within the range with a well-performed reconstruction of $|\frac{q/p err}{q/p}<0.25|$.

\begin{figure}[!ht]
\centering
  \subfloat[\label{fig:rate_loose_CB_qop_dy}]{\includegraphics[width=0.48\textwidth]{figures/analysis_dy/rate_loose_combined_gt20_qoverp}}
  \subfloat[\label{fig:rate_medium_CB_qop_dy}]{\includegraphics[width=0.48\textwidth]{figures/analysis_dy/rate_medium_combined_gt20_qoverp}}

  \subfloat[\label{fig:rate_tight_CB_qop_dy}]{\includegraphics[width=0.48\textwidth]{figures/analysis_dy/rate_tight_combined_gt20_qoverp}}
  \caption{Muon Charge Mis-id rate of CB muons versus $\frac{q/p\ err}{q/p}$ at $Loose$ working point (a), $Medium$ working point (b) and $Tight$ working point (c). The rectangles filled with slashes show the statistical errors in each bin}
\label{fig:rate_CB_qop_dy}
\end{figure}

\begin{table}[!ht]
    \centering

    \begin{tabular}{lcccccc}
        \toprule
        $\frac{q/p\ err}{q/p}$ & \multicolumn{1}{c}{(-1000,\ -5)} & \multicolumn{1}{c}{(-5,\ -2.5)} & \multicolumn{1}{c}{(-2.5,\ -1)} & \multicolumn{1}{c}{(-1,\ -0.75)} & \multicolumn{1}{c}{(-0.75,\ -0.5)} & \multicolumn{1}{c}{(-0.5,\ -0.25)} \\ \hline
        \midrule
        $Loose$  & $8.71\times10^{-1}$ & $6.98\times10^{-1}$ & $4.74\times10^{-1}$ & $3.16\times10^{-1}$ & $1.55\times10^{-1}$ & $2.80\times10^{-1}$ \\ \hline
        $Medium$ & $9.34\times10^{-1}$ & $7.01\times10^{-1}$ & $4.74\times10^{-1}$ & $3.16\times10^{-1}$ & $2.23\times10^{-1}$ & $2.82\times10^{-1}$ \\ \hline
        $Tight$  & $8.92\times10^{-1}$ & $5.08\times10^{-1}$ & $6.09\times10^{-1}$ & $5.16\times10^{-1}$ & $4.52\times10^{-1}$ & $2.44\times10^{-1}$ \\ \hline
    \end{tabular}
    ~\\
    \begin{tabular}{lcccccc}
        \toprule
        $\frac{q/p\ err}{q/p}$ & \multicolumn{1}{c}{(-0.25,\ -0.1)} & \multicolumn{1}{c}{(-0.1,\ 0)} & \multicolumn{1}{c}{(0,\ 0.1)} & \multicolumn{1}{c}{(0.1,\ 0.25)} & \multicolumn{1}{c}{(0.25,\ 0.5)} & \multicolumn{1}{c}{(0.5,\ 0.75)} \\ \hline
        \midrule
        $Loose$  & $2.10\times10^{-2}$ & $2.75\times10^{-6}$ & $1.75\times10^{-6}$ & $1.75\times10^{-2}$ & $4.35\times10^{-1}$ & $2.60\times10^{-1}$ \\ \hline
        $Medium$ & $9.34\times10^{-1}$ & $7.01\times10^{-1}$ & $4.74\times10^{-1}$ & $3.16\times10^{-1}$ & $2.23\times10^{-1}$ & $2.82\times10^{-1}$ \\ \hline
        $Tight$  & $8.92\times10^{-1}$ & $5.08\times10^{-1}$ & $6.09\times10^{-1}$ & $5.16\times10^{-1}$ & $4.52\times10^{-1}$ & $2.44\times10^{-1}$ \\ \hline
    \end{tabular}
    ~\\
    \begin{tabular}{lcccc}
        \toprule
        $\frac{q/p\ err}{q/p}$ & \multicolumn{1}{c}{(0.75,\ 1)} & \multicolumn{1}{c}{(1,\ 2.5)} & \multicolumn{1}{c}{(2.5,\ 5)} & \multicolumn{1}{c}{(5,\ 1000)} \\ \hline
        \midrule
        $Loose$  & $2.92\times10^{-2}$ & $6.69\times10^{-1}$ & $1.30\times10^{-1}$ & $6.68\times10^{-2}$ \\ \hline
        $Medium$ & $2.92\times10^{-2}$ & $6.69\times10^{-1}$ & $1.30\times10^{-1}$ & $6.67\times10^{-2}$ \\ \hline
        $Tight$  & $1.65\times10^{-2}$ & $2.19\times10^{-1}$ & $1.27\times10^{-1}$ & $2.07\times10^{-2}$ \\ \hline
        \bottomrule
    \end{tabular}

    \caption{CB Muon Charge Mis-id rate versus \pt}
    \label{tab:rate_CB_qop_dy}
\end{table}

