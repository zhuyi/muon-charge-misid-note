%-------------------------------------------------------------------------------
% This file provides a skeleton ATLAS note.
\pdfinclusioncopyfonts=1
% This command may be needed in order to get \ell in PDF plots to appear. Found in
% https://tex.stackexchange.com/questions/322010/pdflatex-glyph-undefined-symbols-disappear-from-included-pdf
%-------------------------------------------------------------------------------
% Specify where ATLAS LaTeX style files can be found.
\RequirePackage{latex/atlaslatexpath}
% Comment out the above line if the files are in a central location, e.g. $HOME/texmf.
%-------------------------------------------------------------------------------
\documentclass[NOTE, atlasdraft=true, texlive=2020, UKenglish]{atlasdoc}
% The language of the document must be set: usually UKenglish or USenglish.
% british and american also work!
% Commonly used options:
%  atlasdraft=true|false This document is an ATLAS draft.
%  texlive=YYYY          Specify TeX Live version (2020 is default).
%  coverpage             Create ATLAS draft cover page for collaboration circulation.
%                        See atlas-draft-cover.tex for a list of variables that should be defined.
%  cernpreprint          Create front page for a CERN preprint.
%                        See atlas-preprint-cover.tex for a list of variables that should be defined.
%  NOTE                  The document is an ATLAS note (draft).
%  PAPER                 The document is an ATLAS paper (draft).
%  CONF                  The document is a CONF note (draft).
%  PUB                   The document is a PUB note (draft).
%  BOOK                  The document is of book form, like an LOI or TDR (draft).
%  txfonts=true|false    Use txfonts rather than the default newtx.
%  paper=a4|letter       Set paper size to A4 (default) or letter.

%-------------------------------------------------------------------------------
% Extra packages:
\usepackage[rotating=true, longtable=true]{atlaspackage}
% Commonly used options:
%  biblatex=true|false   Use biblatex (default) or bibtex for the bibliography.
%  backend=bibtex        Use the bibtex backend rather than biber.
%  subfigure|subfig|subcaption  to use one of these packages for figures in figures.
%  minimal               Minimal set of packages.
%  default               Standard set of packages.
%  full                  Full set of packages.
%-------------------------------------------------------------------------------
% Style file with biblatex options for ATLAS documents.
\usepackage{atlasbiblatex}
% Commonly used options:
%  backref=true|false    Turn on or off back references in the bibliography.

% Package for creating list of authors and contributors to the analysis.
\usepackage{atlascontribute}

% Useful macros
\usepackage{atlasphysics}
% See doc/atlas_physics.pdf for a list of the defined symbols.
% Default options are:
%   true:  journal, misc, particle, unit, xref
%   false: BSM, hepparticle, hepprocess, hion, jetetmiss, math, process,
%          other, snippets, texmf
% See the package for details on the options.

% Macro to add to-do notes (for several authors). Uses the todonotes package.
% \ATLnote{JS}{Jane}{green!20}{green!50!black!60}
% add macros \JSnote and \JSinote for notes in the margin and inline.
% Set output=false in order not to print out the notes.
% Comment this out for the final version of the note.
\usepackage[output=true]{atlastodo}

% Files with references for use with biblatex.
% Note that biber gives an error if it finds empty bib files.
\addbibresource{muon_charge_misid_note.bib}
\addbibresource{bib/ATLAS.bib}
\addbibresource{bib/CMS.bib}
\addbibresource{bib/ConfNotes.bib}
\addbibresource{bib/PubNotes.bib}
\addbibresource{bib/ATLAS-useful.bib}

% Paths for figures - do not forget the / at the end of the directory name.
\graphicspath{{logos/}{figures/}}

% Add your own definitions here (file muon_charge_misid_note-defs.sty).
\usepackage{muon_charge_misid_note-defs}

%-------------------------------------------------------------------------------
% Generic document information.
%-------------------------------------------------------------------------------

% Title, abstract and document.
\input{muon_charge_misid_note-metadata}
% Author and title for the PDF file.
\hypersetup{pdftitle={ATLAS document},pdfauthor={The ATLAS Collaboration}}

%-------------------------------------------------------------------------------
% Content
%-------------------------------------------------------------------------------
\begin{document}

\maketitle

\tableofcontents

% List of contributors - print here or after the Bibliography.
% \PrintAtlasContribute{0.30}
% \clearpage

% List of to-do notes.
% \listoftodos

\clearpage
%-------------------------------------------------------------------------------

\section{Introduction}
\label{sec:introduction}
\input{src/introduction}

\section{Muon Reconstruction in ATLAS Experiment}
\label{sec:atlas}

\subsection{ATLAS Detector}
\label{sec:atlas_detector}
\input{src/atlas/atlas_detector}

\subsection{Muon Reconstruction in ATLAS Experiment}
\label{sec:atlas_muons}
\input{src/atlas/atlas_muons}

\section{Data and Simulated Samples}
\label{sec:samples}
\input{src/samples}

%-------------------------------------------------------------------------------

\section{Muon Mis-id Rate Estimation Using Drell-Yan Samples}
\label{sec:analysis_dy}

\subsection{Drell-Yan Process}
\label{sec:drell_yan}
\input{src/analysis_dy/drell_yan}

\subsection{Basic Distributions of Muons in Drell-Yan samples}
\label{sec:dy_basic}
\input{src/analysis_dy/drell_yan_basic}

\FloatBarrier

\subsection{Muon Charge Mis-id Rate Estimation Based on Truth-Reco Matching}
\label{sec:dy_rate}
\input{src/analysis_dy/drell_yan_rate}

\FloatBarrier

%\subsection{Muon Charge Mis-id Rate Estimation on Detector Misalignment}
%\label{sec:dy_misalignment}
%\input{src/analysis_dy/drell_yan_misalignment}

\subsection{Muon Charge Mis-id Rate Validation with Truth Muons}
\label{sec:dy_truth_rate}
\input{src/analysis_dy/drell_yan_truth_rate}

\FloatBarrier

\subsection{Muon Charge Mis-id Rate Validation with Charges of Tracks}
\label{sec:dy_track_rate}
\input{src/analysis_dy/drell_yan_track_rate}

\FloatBarrier

\section{Muon Mis-id Rate Estimation Using $Z\rightarrow\mumu$ Samples}
\label{sec:analysis_zmumu}

\subsection{Tag and Probe Method}
\label{sec:tag_probe}
\input{src/analysis_zmumu/tag_probe}

\subsection{Basic Distributions of Tag and Probe Muons}
\label{sec:tag_probe_basic}
\input{src/analysis_zmumu/tag_probe_basic}

\subsection{Muon Charge Mis-id Estimation Based on Tag and Probe Method}
\label{sec:tag_probe_rate}
\input{src/analysis_zmumu/tag_probe_rate}

\FloatBarrier

\subsection{First Glance at Charge Mis-id Rate in Data Samples}
\label{sec:tag_probe_rate_data}
\input{src/analysis_zmumu/tag_probe_rate_data}

\FloatBarrier

%-------------------------------------------------------------------------------

%You can find some text snippets that can be used in papers in \texttt{latex/atlassnippets.sty}.
%To use them, provide the \texttt{snippets} option to \texttt{atlasphysics}.

%-------------------------------------------------------------------------------

% All figures and tables should appear before the summary and conclusion.
% The package placeins provides the macro \FloatBarrier to achieve this.
% \FloatBarrier

%-------------------------------------------------------------------------------
\section{Conclusion}
\label{sec:conclusion}
\input{src/conclusion}
%-------------------------------------------------------------------------------

%Place your conclusion here.

%-------------------------------------------------------------------------------
% If you use biblatex and either biber or bibtex to process the bibliography
% just say \printbibliography here.
\printbibliography
% If you want to use the traditional BibTeX you need to use the syntax below.
% \bibliographystyle{obsolete/bst/atlasBibStyleWithTitle}
% \bibliography{muon_charge_misid_note,bib/ATLAS,bib/CMS,bib/ConfNotes,bib/PubNotes}
%-------------------------------------------------------------------------------

%-------------------------------------------------------------------------------
% Print the list of contributors to the analysis.
% The argument gives the fraction of the text width used for the names.
%-------------------------------------------------------------------------------
\clearpage
%The supporting notes for the analysis should also contain a list of contributors.
%This information should usually be included in \texttt{mydocument-metadata.tex}.
%The list should be printed either here or before the Table of Contents.
\PrintAtlasContribute{0.30}

%-------------------------------------------------------------------------------
\clearpage
\appendix
\part*{Appendices}
\addcontentsline{toc}{part}{Appendices}
%-------------------------------------------------------------------------------

% In an ATLAS note, use the appendices to include all the technical details of your work
% that are relevant for the ATLAS Collaboration only (e.g.\ dataset details, software release used).
% This information should be printed after the Bibliography.

\input{src/appendices}

\end{document}
